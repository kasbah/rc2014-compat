#!/usr/bin/env bash
cp -r rc2014-template$2 $1
cd $1
rm -r meta rc2014-template$2-backups
for i in kicad_pcb kicad_prl kicad_pro kicad_sch
do
  mv rc2014-template$2.$i $1.$i
  sed -i "s/rc2014-template$2.kicad/$1.kicad/" $1.$i
done
